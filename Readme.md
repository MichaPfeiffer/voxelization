Voxelization
==================================

This repository contains code to create a regular grid and
- compute the distance field of a given mesh for each point on the grid
- compute the signed distance field of a given (closed) mesh for each point on the grid
- interpolate a displacement field onto each point of the grid

Prerequisites
==================================

- python3
- numpy
- vtk python bindings (python3 -m pip install vtk)
- pcl python bindings (sudo apt install python3-pcl)

Supported file types:
==================================
Input file types can be one of:
- stl
- ply
- obj
- vtk
- vtu
- vtp
- txt
- csv
- pcd

Output files are VTK's structured grids (.vts).

Usage
==================================
To calculate the distance field (DF) of the point cloud 'cloud.pcd' and save it in a grid called 'grid.vts':

```
python3 voxelize.py --DF cloud.pcd --output_grid grid.vts
```

To calculate the signed distance field instead, pass '--SDF' instead of '--DF'. Note that for this, the mesh must be a closed manifold. Thus, a point cloud (.pcd) won't work, we need to instead pass a triangle mesh:

```
python3 voxelize.py --SDF mesh.stl --output_grid grid.vts
```

To interpolate a displacement field onto this grid, you can used voxelize_displacement.py. This works in one of two modes:
1. The first mode assumes you have an initial, original volume mesh and its deformed counterpart Both need to have the same number of points (and cells) and there is a 1 to 1 correspondence between these points and cells. In this case, pass the meshes using the '--mesh_initial' and '--mesh_deformed' arguments.
2. The second mode assumes you have a single deformed mesh which is the deformed state, and this mesh has an array called 'displacement' which holds the (x,y,z) displacement vector for each point. In this mode, the algorithm will first calculate the initial mesh state by "un-deforming" the mesh. Use the argument '--mesh' to pass the mesh to the script.

For more details, try `python3 voxelize.py -- help` or `python3 voxelize_displacement.py --help`

Transformation
==================================

By default, the algorithm creates a grid around the origin and operates in meters. The default grid size is 0.3m. If your data is not centered around the origin or does not fit into this size, you can use the --scale_input, --move_input or --center_input options to move the mesh. Using these flags also saves the transform in the 'field data' of the grid. One way to use this is to apply --center_input when voxelizing the preoperative mesh and then using --reuse_transform when voxelizing the intraoperative mesh. This ensures that the intraoperative mesh is transformd in the same way as the preoperative mesh was.
When you later want to move the displacement field back to the original position, simply look at the transformation stored in the grid's 'field data'.

