import os
import math
import random
import re
import subprocess

# Check if string is a valid path. If not, try to create it. If not, fail.
def path(string):
    if os.path.isdir(string):
        return string
    else:
        try:
            os.makedirs(string)
            return string
        except:
            raise NotADirectoryError(string)

# Check if string is a valid file. If not, fail.
def filepath(string):
    if os.path.isfile(string):
        return string
    else:
        raise FileNotFoundError(string)

def directoryForIndex( index, outdir ):
    d = os.path.join( outdir, "{:05d}".format(index))
    if not os.path.exists( d ):
        os.makedirs( d )
    return d

def getSurfaceMeshName( directory ):
    meshManipFileName = os.path.join( directory, "surface_manip.stl" )
    if os.path.exists( meshManipFileName ):
        return "surface_manip.stl"
    return "surface.stl"


def findFiles( directory, pattern ):
    """
    Find all files matching a pattern in directory.
    """

    fileList = []
    for f in os.listdir( directory ):
        fullpath = os.path.join( directory, f )
        if os.path.isfile( fullpath ):
            if re.match( pattern, f ):
                fileList.append( f )
    return fileList


def removeFiles( directory, pattern ):
    """
    Remove all files in directory matching pattern
    """
    
    files = findFiles( directory, pattern )
    for f in files:
        os.remove( os.path.join( directory, f ) )

    
def logRunInfo( outputFolder, args ):
    result = subprocess.run(['git', 'rev-parse', 'HEAD'], stdout=subprocess.PIPE)
    sha = result.stdout.decode('utf-8')
    result = subprocess.run(['hostname'], stdout=subprocess.PIPE)
    hostname = result.stdout.decode('utf-8')

    if not os.path.exists( outputFolder ):
        os.makedirs( outputFolder )

    print("Saving results to: {}".format(outputFolder))

    runinfoFile = os.path.join( outputFolder, "RunInfo" )
    print("Saving run info to {}".format(runinfoFile) )
    with open( runinfoFile, "w" ) as f:
        f.write("Git commit:\t" + str(sha))
        f.write("Hostname:\t" + str(hostname))
        f.write("\nArguments:\n")
        for key,value in vars(args).items():
            f.write("\t" + key + "\t" + str(value) + "\n")

